/*
 * File: "cudainfo.c"
 */

//----------------------------------------------------------------------------
#include <stdio.h>
#include <cuda_runtime.h>
//#include <cufft.h>
//#include <cufftXt.h>
//#include <helper_cuda.h>
//#include <helper_functions.h>
//----------------------------------------------------------------------------
static void HandleError(cudaError_t err,
                        const char *file,
                        int line)
{
  if (err != cudaSuccess)
  {
    printf("%s in %s at line %d\n",
           cudaGetErrorString(err), file, line);
    exit(EXIT_FAILURE);
  }
}
//----------------------------------------------------------------------------
#define HANDLE_ERROR(err) (HandleError(err, __FILE__, __LINE__ ))
//----------------------------------------------------------------------------
int main()
{
  int i;
  printf("GPU information:\n");

  cudaDeviceProp prop;
  int dev_cnt;
  HANDLE_ERROR(cudaGetDeviceCount(&dev_cnt));
  printf("  Detected %d devices\n", dev_cnt);
  for (i = 0; i < dev_cnt; i++)
  {
    HANDLE_ERROR(cudaGetDeviceProperties(&prop, i));
    printf("    Device #%d:\n", i );
    printf("      General information for device #%d\n", i);
    printf("        - name                      : %s\n", prop.name); // ASCII идентификатор
    printf("        - compute capability        : %d.%d\n", prop.major, prop.minor);
    printf("        - clockRate [MHz]           : %.3f\n", prop.clockRate / 1000.);
    printf("        - integrated                : %d\n", prop.integrated);
    
    // флаг, показывающий, может ли устройство одновременно
    // выполнять функцию CudaMemcpy() и ядро
    printf("        - deviceOverlap             : " );
    if (prop.deviceOverlap) printf( "enabled\n" );
    else                    printf( "disabled\n");

    // флаг, показывающий, существует ли ограничение на время исполнения ядер устройством
    printf("        - kernelExecTimeoutEnabled  : " );
    if (prop.kernelExecTimeoutEnabled) printf("enabled\n");
    else                               printf("disabled\n");

    printf("      Memory informationfor device #%d\n", i);
    printf("        - totalGlobalMem [B]        : %ld\n", prop.totalGlobalMem);
    printf("        - totalConstMem  [B]        : %ld\n", prop.totalConstMem);
    printf("        - memPitch [B]              : %ld\n", prop.memPitch);
    printf("        - textureAlignment          : %ld\n", prop.textureAlignment);
    printf("      MP information for device #%d\n", i);
    printf("        - multiProcessorCount       : %d\n", prop.multiProcessorCount);
    printf("        - sharedMemPerBlock [B]     : %ld\n", prop.sharedMemPerBlock);
    printf("        - regsPerBlock              : %d\n", prop.regsPerBlock);
    printf("        - warpSize                  : %d\n", prop.warpSize);
    printf("        - maxThreadsPerBlock        : %d\n", prop.maxThreadsPerBlock);
    printf("        - maxThreadsDim             : (%d, %d, %d)\n",
           prop.maxThreadsDim[0], prop.maxThreadsDim[1], prop.maxThreadsDim[2]);
    printf("        - maxGridSize               : (%d, %d, %d)\n",
           prop.maxGridSize[0], prop.maxGridSize[1], prop.maxGridSize[2]);

    printf("        - canMapHostMemory          : %d\n", prop.canMapHostMemory);
    printf("        - computeMode               : %d\n", prop.computeMode);
    printf("        - maxTexture1D              : %d\n", prop.maxTexture1D);
#if 0
    printf("        - maxTexture2D              : (%ld, %ld)\n",
           prop.maxTexture2D[0], prop.maxTexture2D[1]);
    printf("        - maxTexture3D              : (%ld, %ld, %ld)\n",
           prop.maxTexture3D[0], prop.maxTexture3D[1], prop.maxTexture3D[2]);
    printf("        - maxTexture2DArray         : (%ld, %ld, %ld)\n",
           prop.maxTexture2DArray[0], prop.maxTexture2DArray[1], prop.maxTexture2DArray[2]);
    printf("        - concurentKernels          : %d\n", prop.concurentKernels);
#endif
  } // for(i..
}
//----------------------------------------------------------------------------

/*** end of "cudainfo.c" ***/

