#!/bin/bash

make clean

rm -f CMakeCache.txt
rm -rf CMakeFiles
rm -rf cmake_install.cmake
rm -f Makefile
