NVIDIA GeForce MX450 2GB on Lenovo ThinkPad E14 20TA034RT
=========================================================
GPU information:
  Detected 1 devices
    Device #0:
      General information for device #0
        - name                      : GeForce MX450
        - compute capability        : 7.5
        - clockRate [MHz]           : 1575.000
        - integrated                : 0
        - deviceOverlap             : enabled
        - kernelExecTimeoutEnabled  : enabled
      Memory informationfor device #0
        - totalGlobalMem [B]        : 1969684480
        - totalConstMem  [B]        : 65536
        - memPitch [B]              : 2147483647
        - textureAlignment          : 512
      MP information for device #0
        - multiProcessorCount       : 14
        - sharedMemPerBlock [B]     : 49152
        - regsPerBlock              : 65536
        - warpSize                  : 32
        - maxThreadsPerBlock        : 1024
        - maxThreadsDim             : (1024, 1024, 64)
        - maxGridSize               : (2147483647, 65535, 65535)
        - canMapHostMemory          : 1
        - computeMode               : 0
        - maxTexture1D              : 131072

NVIDIA GeForce MX350 on Acer Swift 3 SF314-57G-764E
===================================================
GPU information:
  Detected 1 devices
    Device #0:
      General information for device #0
        - name                      : GeForce MX350
        - compute capability        : 6.1
        - clockRate [MHz]           : 936.500
        - integrated                : 0
        - deviceOverlap             : enabled
        - kernelExecTimeoutEnabled  : disabled
      Memory informationfor device #0
        - totalGlobalMem [B]        : 2099904512
        - totalConstMem  [B]        : 65536
        - memPitch [B]              : 2147483647
        - textureAlignment          : 512
      MP information for device #0
        - multiProcessorCount       : 5
        - sharedMemPerBlock [B]     : 49152
        - regsPerBlock              : 65536
        - warpSize                  : 32
        - maxThreadsPerBlock        : 1024
        - maxThreadsDim             : (1024, 1024, 64)
        - maxGridSize               : (2147483647, 65535, 65535)
        - canMapHostMemory          : 1
        - computeMode               : 0
        - maxTexture1D              : 131072

