cmake_minimum_required(VERSION 3.10 FATAL_ERROR)

set(CMAKE_CUDA_COMPILER "/usr/local/cuda/bin/nvcc")
#set(CMAKE_CUDA_COMPILER "nvcc")

project(cudainfo LANGUAGES C CUDA)

#if(CMAKE_GENERATOR MATCHES "Unix Makefiles")
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_CURRENT_SOURCE_DIR}/bin)
#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CMAKE_CURRENT_SOURCE_DIR}/bin)
#endif(CMAKE_GENERATOR MATCHES "Unix Makefiles")

#set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
#set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE ${CMAKE_CURRENT_SOURCE_DIR}/lib)
#set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG ${CMAKE_CURRENT_SOURCE_DIR}/lib)
#set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
#set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${CMAKE_CURRENT_SOURCE_DIR}/lib)
#set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${CMAKE_CURRENT_SOURCE_DIR}/lib)

# Find includes in corresponding build directories
#set(CMAKE_INCLUDE_CURRENT_DIR ON)

#add_subdirectory(./qqq)

#target_link_libraries(${PROJECT_NAME} m rt cufft cudart_static)
#target_link_libraries(${PROJECT_NAME} m rt)

include_directories(
	${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES}
  #/usr/local/cuda/targets/x86_64-linux/include
  #/usr/local/cuda/samples/common/inc/
)

set(Sources cudainfo.cu)

add_executable(${PROJECT_NAME} ${Sources})

#add_library(${PROJECT_NAME} STATIC ${Sources})

#add_dependencies(${PROJECT_NAME} some_lib)

